.. _Actions:
Actions
=======


Objetivo
--------

Gerar um conjunto de instruções que podem ser movimentos, falas e expressões do Robô
Diz respeito à programação do robô

.. figure:: .attachments/listagem-acao.png

Funcionalidades
---------------

- Seleção do Tinbot - um dropdown lista todos os Times que o usuário logado tem Acesso

.. figure:: .attachments/image-5efcdbf0-75d2-4b06-8a9a-71c5aa912506.png

.. figure:: .attachments/image-e20c9fde-1a2f-4614-9e20-0e4fb62a9d3f.png


- Listagem de ações - são listadas todas as ações cadastrasdas no Time selecionado

.. figure:: .attachments/image-cb9e7ef5-1279-4c96-b993-cef1d41bf929.png


- Adição de TicoTico - direciona para a tela de inclusão de uma nova ação do tipo TicoTico
Botão ativo somente após seleção de um time

.. figure:: .attachments/image-cf5bce96-8652-4447-847b-3c1569de1558.png 

- Adição de Javascript - direciona para a tela de inclusão de uma nova ação do tipo Javascript
Botão ativo somente após seleção de um time

.. figure:: .attachments/image-139bf594-8b7e-4ec9-b0f4-458b9a75d361.png

Tipos de Actions
----------------

Javascript
~~~~~~~~~~
Objetivo
********
Programar uma ação utilizando a linguagem Javascript, podendo fazer consultas a APIs externas e outras implementações e executar o resultado no Robô em formato de expressões e/ou mensagens.

.. figure:: .attachments/editando-js-acao.png

Funcionalidade
**************

Edição de código com editor "Moncao Editor"

.. figure:: .attachments/image-8cae3f3b-6fb4-4114-b64c-2694e943820e.png


Lista de ajuda com exemplos e instruções

** Cada botão abre um modal com o guia em questão

** O modal vai ter o mesmo componente de edição de código 

.. figure:: .attachments/lista-instrucoes-js-acao.png

.. _ticotico:
TicoTico
~~~~~~~~

Objetivo
********
Criar uma ação utilizando texto puro, podendo conter caracteres especiais, para ser executado no robô com a intenção de criar expressões e/ou mensagens.

.. figure:: .attachments/editando-ticotico-acao.png


Funcionalidade
**************

Edição de código com editor "Moncao Editor"

.. figure:: .attachments/image-08906218-58f8-454f-9696-4efaa7e717d2.png


Lista com os comandos suportados (Instruções)

.. figure:: .attachments/image-44d5caca-889a-4422-a413-440b5aff463e.png

Testador (Ticotico ou Javascript)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Testador tem a finalidade de executar as ações no Robô antes de salvá-las. 
Para isso, deve-se selecionar um Robô na lista e o mesmo deve estar "online".

**O Botão "play" executa o código no Folha selecionado

**Pode-se adicionar parâmetros a execução da ação, para isso ele deve ser adicionado também no editor

.. figure:: .attachments/testador-acao.png