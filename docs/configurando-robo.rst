Configurando Robô
=================

Apesar de já vir configurado pronto para o uso, há algumas alterações que podem ser feitas visando o melhor uso. Tais configurações podem ser alteradas na opção disponível na tela de Tinbots.

.. figure:: .attachments/listagem-times-config-user.png

Ao abrir a tela de configuração, as opção disponíveis para alterar são **volume**, **ativadores** e **serviço de conversação**.

De modo geral é bem simples, basta alterar ou inserir os valores e salvar. Após isso o Robô irá avisar que houve atualização de parâmetros. É aconselhável reeinicar o Robô.

Configurando ativadores
-----------------------

Selecione a opção **Personalizar** para alterar/criar e utilizar novos ativadores.

.. note::

	Ao clicar em **Adicionar Ativador** irá aparecer duas caixas de textos:
		- Hotwords: palavras/frases que ao ser pronunciadas ativam o Robô vazendo ele esperar uma resposta ou retornando uma resposta. Um exemplo de Hotwords é "Helo Tinbot".
		- Texts: palavras/frases que são respostas para os Hotwords cadastrados. Diferente de Hotwords, os Texts não são obrigatórios.

		
Insira os Hotwords na caixa de texto, sendo cada palava/frase em uma linha. Logo depois, caso tenha, insira os Texts cada palavra/frase em uma linha.

Pode existir vários grupos de Hotwords/Texts. Lembrando que o Text de um grupo nunca será a resposta de outro grupo.

.. note::

	A opção **Limpar** é usada quando não deseja herdar Ativadores de outros times que são "parentes" do time atual. 
	Exemplo: há ativadores para Factory e o tinbot1000, time que está sendo alterado no momento, está logo abaixo e não deseja herdar os ativadores de Factory.
	

Serviço de conversação
----------------------

Selecione a opção **Personalizar** para alterar/criar e utilizar um serviço de conversação.

.. note::

	**Comparar com as triggers primeiro** é uma opção existente no tipo de conversa **IBM Watson** e **Conversa Genérica**. Quando selecionado, antes de executar o tipo de conversa, ele verifica se existe triggers cadastrada e executa o comando. 
	Caso nao tenha triggers cadastrada é executado o tipo de conversa escolhido.

Escolha o tipo de conversa selecionando uma das três opções: **IBM Watson**, **Conversa Genérica** ou **Comando por Voz**.

Cada uma tem um propósito diferente, sendo:
	- IBM Watson para conversas direcionada a Bots, ou seja, uma integração com um serviço de conversa já existente e criado no Watson
	- Conversa genérica para conversas que desejam repostas contidas na internet, mais especificamente no Google
	- Comando por voz para conversas que desejam respostas cadastradas na trigger somente

.. warning::

	Para o uso do IBM Watson é necessário preencher os parâmetros de acesso ao bot. Caso não seja preenchido não irá funcionar.
	
	.. figure:: .attachments/configurando-watson.png
	
