Variáveis Globais
=================

Objetivo
--------

Ter em local centralizado a opção de variáveis para uso global, fazendo com que variáveis utilizadas em triggers ou action não necessitem ser passadas a todo momento.

.. figure:: .attachments/image-20cdc6a8-c562-4e04-9fab-8ffb067d7631.png

Funcionalidade
--------------

Toda edição, adição e exclusão é feita na tela de listagem.

.. figure:: .attachments/editando-variavel-global.png