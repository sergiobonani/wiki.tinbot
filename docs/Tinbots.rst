Tinbots
=======

Objetivo
--------
Listar todas as Famílias de todos os Times e Robôs que o usuário logado tem permissão.
Sendo possível acessar através desta tela as **configurações**, **ações**, **triggers**, **logs** e **variáveis globais**.

.. figure:: .attachments/image-362e8ba3-ced8-4b06-b6f7-6f4630abd504.png

Funcionalidades
---------------
A listagem seguirá a ordem da hierarquia dos times. Podendo ser Robôs os itens de menores níveis.

Os botões das funções serão vísiveis apenas para os times quais o usuário tem acesso.

Configuração XML/JSON
~~~~~~~~~~~~~~~~~~~~~

.. figure:: .attachments/image-466bee20-bf35-4b70-b344-772deacada4b.png 

Direciona para o modal de edição das configurações XML e Json do time selecionado.
Nesse modal é possível ajustar as configurações que serão usadas no Robô. Tais como Ativadores, Volume, Frases para determinadas condições, entre outros.

.. note:: 

    A opção JSON está sendo descontinuada, dando lugar a configuração XML.

.. figure:: .attachments/tela-configuracao-xml.png

Config Users
~~~~~~~~~~~~

.. figure:: .attachments/botao-config-user.png

Direciona para a tela Configurações do time, onde o usuário poderá selecionar e adicionar configurações de sua preferência, que incluem alterar volume, ativadores e serviço de conversa.

Actions
~~~~~~~

.. figure:: .attachments/image-197f792b-6584-448d-a1b4-212d28983e6c.png 

Direciona para a tela de :ref:`Actions<Actions>` do Time.

Triggers
~~~~~~~~

.. figure:: .attachments/image-a5f64af4-66c4-4352-9582-c65621b68ff1.png 

Direciona para a tela de :ref:`Triggers<Triggers>` do time

TicoTico
~~~~~~~~

.. figure:: .attachments/image-8e4d09e1-059d-41dd-b9eb-a3d826eb079a.png 

Abre tela para a execução de uma :ref:`Ação TicoTico<ticotico>` manualmente.