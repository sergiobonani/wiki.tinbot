Configurando Webhook
====================

Uma opção boa para usar com o Robô é a utilização de Webhook para uso externo e integração com outros sistemas. Sendo possível enviar comandos diretamente ao Robô sem a necessidade de ter uma ação cadastrada.

Para criar um webhook é necessário ter permissão de administrador do time que contém o Robô. Caso tenha permissão, vá em Gerenciamento > Hooks.
Selecione um time e clique em adicionar.

.. figure:: .attachments/tela-hook.png

Abrirá um modal onde será necessário dar um nome para esse novo hook. Após isso é só salvar e o acesso estará pronto.

.. figure:: .attachments/criando-hook.png

Você poderá criar quantos hooks forem necessários e excluí-los quando for preciso. Lembre-se que ao excluir o acesso por aquele link não ficará mais disponível.

Após ter salvo a criação do hook, será exibido na listagem uma URL que iremos usar para comunicar diretamente com o Robô.

.. figure:: .attachments/criando-hook.png

Uso da URL gerada no hook
-------------------------

Com a URL gerada temos a opção de **enviar mensagem** ou **chamar uma ação**. Abaixo como podemos usar a URL gerada.

.. note::

	Além disso, temos que nos atentar se o Hook criado foi para o time do Robô ou para algum parente do mesmo.
	
Enviando mensagem
~~~~~~~~~~~~~~~~
Para enviar uma mensagem diretamente ao Robô, é necessário passar como parâmetro ``ticotico`` com a mensagem que deseja

``GET https://betaapi.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?ticotico=Olá pessoas``

Enviando mensagem e passando o nome do robô
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem onde o Hook criado foi para um time, que não seja o robô, é necessário passar como parâmetro ``robotname`` e ``ticotico`` com a mensagem.

``GET https://betaapi.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&ticotico=Olá pessoas``

Enviando comando para executar uma ação
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem diretamente ao Robô, é necessário passar como parâmetro ``actioncode`` com o código da ação

``GET https://betaapi.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?actioncode=123``

Enviando comando para executar uma ação e passando o nome do robô
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar uma mensagem onde o Hook criado foi para um time, que não seja o robô, é necessário passar como parâmetro ``robotname`` e ``actioncode`` com o código da ação.

``GET https://betaapi.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&actioncode=123``

Enviando comando passando parâmetros para a ação
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Para enviar um comando que seja necessário passar parâmetros será preciso enviá-los pelo corpo da requisição. 

``POST https://betaapi.tinbot.club/hooks/302f7e8e-7137-486a-99d3-522f6f58fd08?robotname=tinbot1000&actioncode=123``

Com o corpo da requisição sendo:

.. code-block:: json

	{
		"parametro" : "valor"
	}
	
.. code-block:: curl

	curl -X POST \
	  'https://betaapi.tinbot.club/hooks/65fd219a-c113-40da-9e68-1ddc88c6137d?robotname=tinbot1012&actioncode=226&name=ol%C3%A1%20derpino' \
	  -H 'cache-control: no-cache' \
	  -H 'content-type: application/json' \
	  -H 'postman-token: 85801922-fc7f-d2f6-52e6-6c7791fc3eb9' \
	  -d '{
		"name": "roberval"
	}
	'

