Conhecidos
==========

Objetivo
--------

Edição, inserção e exclusão de fotos de pessoas para um time, com o objetivo de possuir uma base para identificação da mesma por parte do Robô. Sendo assim, com ação e trigger cadastrado
para utilizar tais fotos, ao executar o comando o Robô será capaz de identificar e conversar com a pessoa.

.. figure:: .attachments/listagem-conhecidos.png