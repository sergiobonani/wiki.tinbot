Criando uma consersa com o Robô
===============================

Criando uma ação
----------------

Esse é o primeiro passo para o Tinbot possa interagir e responder perguntas cadastradas nele. Nesse exemplo iremos criar uma ação simples, sendo assim, caso
queira implementar outras ações vá em :ref:`Actions <Actions>` para saber mais. 

Na tela de Actions, escolha um time em que deseja criar a ação. Lembrando que para essa ação seja executada dentro do Robô você deve escolher um time que seja diretamente ligado ao robô.
Para exemplificar melhor, olhe a estrutura abaixo:

.. figure:: .attachments/listagem-times.png

Na imagem vemos que todos os times estão abaixo do **Tinbot Factory**. Se fossemos levar em consideração essa estrutura e na tela de Actions criarmos uma ação para o time **Beta-Team**, todos
os times abaixo estariam aptos a executar a ação criada. Mas se formos mais especificos e criarmos uma ação, por exemplo, para o **Tinbot1000**, somente esse time estaria apto para executar a ação.
Isso se deve ao fato dele se encontrar no nivel mais baixo e não possuir "filhos" abaixo dele.

Voltando para a tela de Actions, escolhido o time em que deseja criar a ação, vamos escolher o tipo de ação. No nosso caso, iremos usar o tipo **ticotico**.

.. figure:: .attachments/tela-action-escolha-time.png

Ao clicar em **TicoTico**, irá abrir a tela de criação de Action. Nesse momento iremos dar um Nome e, caso queria, uma descriação. Em seguida, no campo de livre digitação (caixa de texto escura)
iremos escrever o comando em que o Robô irá executar ao chamar essa action.

.. figure:: .attachments/criando-uma-action.png

.. note::

	Após isso e antes de salvar, temos a opção de executar o comando em um robô que escolhermos para verificar se está correto. A opção para fazer isso se chama **Testador** e se encontra do lado direito
	da tela. Se o Robô estiver perto de você, e você o selecionou para executar o comando, ele deverá executar com sucesso.

	.. figure:: .attachments/testando-action.png
	

Após criar a ação, basta salvar.

Criando uma trigger
-------------------

Aqui, iremos criar um evento que será responsável por disparar a ação, cadastrada anteriormente, ao Tinbot. 
Caso queira implementar outras de Trigger vá em :ref:`Triggers <Triggers>` para saber mais.

Antes de cadastrar uma Trigger, precisamos escolher um time e a regra é a mesm explicada no passo anterior. Então, dessa forma, após escolher o time onde deseja cadastrar a Trigger, iremos escolher o tipo de trigger que queremos.
Após escolher o tipo o botão adicionar irá ficar disponível para ser clicado.

.. figure:: .attachments/selecao-tipo-trigger.png

Para nosso primeiro cadastro iremos utilizar a Trigger por voz.
No modal de cadastro devemos preencher tanto o campo Condição como o campo ação. No campo condição, o que deve ser colocado é como nos comunicaremos com o Robô, escolheremos a condição "Bom dia". 
Já no campo Ação iremos colocar a ação cadastrada no passo anterior.

.. figure:: .attachments/criando-uma-trigger.png

Quando salvar o cadastro o Robô deverá alertar sobre a atualização de trigger. Mas isso só acontecerá se a trigger for criada diretamente para o time dele.

Conversando com o Tinbot
------------------------

Após criar uma ação e uma trigger, chegou a hora de interagir com o robô.

Como é a nossa primeira interação e o robô ainda está com as configurações de fábrica, iremos utilizar o ativador "Hello Tinbot" para comunicar com ele.

E o que seria um Ativador? Ativador é uma palavra ou frase que quando pronunciada o Robô entenderá que você está querendo comunicar com ele. 
Com isso ele irá ativar o microfone para ouvir o que você deseja e assim executar uma ação, caso a mesma esteja cadastrada.

Quando falarmos "Hello Tinbot" ele irá emitir um sinal "bip" e após isso é só falar "Boa tarde". 
O Robô irá interpretar o que foi dito e irá procurar algo que se encaixe nessa fala. Como cadastramos anteiormente uma trigger "Boa tarde", ele irá executar a ação vinculada a trigger
e responderá "Boa tarde humano".

Toda vez que você se comunicar com o Robô ele deverá interpretar o que foi dito e responder com uma ação vinculada a essa condição.

Bom uso :)

