.. _Triggers:
Triggers
========

Objetivo
--------

Criar eventos responsáveis por disparar ações no Robô. Podendo ser dos tipos **Voz**, **Agendamento** ou **Pooling (JS)**.

Funcionalidades
---------------

Na listagem de Triggers é possível visualizar todas as cadastradas para o time selecionado e seus superiores. Além disso, pode-se inserir, excluir e editar as triggers do time. No caso de triggers de superiores, só é permitido ativar/desativar a mesma.

.. figure:: .attachments/tela-triggers.png

Para **seleção de time** um dropdown lista todos os Times que o usuário logado tem Acesso

.. figure:: .attachments/image-e20c9fde-1a2f-4614-9e20-0e4fb62a9d3f.png

Para **listagem de triggers** são listados todas as Triggers cadastradas para o time e seus superiores. No caso de triggers de superiores, a cor será diferente. E a cor se diferencia quando a trigger estiver ativa ou desativada no time de origem.

Quando azul mais claro, trigger desativada no time de origem

.. figure:: .attachments/trigger-desativada-pelo-time-origem.png

Quando azul mais escuro, trigger ativa no time de origem

.. figure:: .attachments/trigger-ativa-no-time-de-origem.png

Dessa forma a listagem completa ficará assim:

.. figure:: .attachments/listagem-trigger.png

Adição de nova trigger
----------------------

Para adicionar uma nova trigger é necessário selecionar primeiramente um tipo de trigger. Após seleção o botão 'Adcionar' ficará disponível para ser clicado

.. figure:: .attachments/botao-adicionar-e-tipos-triggers.png
.. figure:: .attachments/botao-adicionar-ativo-e-tipos-triggers.png

	
Tipos de trigger
~~~~~~~~~~~~~~~~

Voz
***
Esse tipo consiste numa trigger simples onde é passado uma condição e selecionado uma ação para a mesma. Ou seja, quando esse tipo exister e for chamada no robô, irá responder com a ação selecionada.

.. figure:: .attachments/trigger-voz.png

JSPooling
*********
Trigger codificada com código javascript executado de tempos em tempos que avalia uma condição qualquer e, quando satisfeita a condição, dispara a ação correspondente

.. figure:: .attachments/trigger-jspooling.png

Agendamento
***********
Esse tipo consiste em disparar uma ação no robô no período agendado. Podendo atribuir uma ação à um horário do dia, à um dia da semana, etc.

.. figure:: .attachments/trigger-agendamento.png

Imprimir
--------

O botão imprimir tem a finalidade de exibir em formato PDF todas as Triggers disponíveis no time acessado, podendo se basear nos filtros selecionados.

.. figure:: .attachments/botao-imprimir-trigger.png

.. figure:: .attachments/listagem-imprimir-trigger.png