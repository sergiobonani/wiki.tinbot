Gerenciamento de Hooks
======================

Objetivo
--------
Tem como finalidade a utilização de Webhook para uso externo e integração com outros sistemas. 
Sendo possível enviar comandos diretamente ao Robô sem a necessidade de autenticação e com a opção de não precisar de uma ação cadastrada.

.. figure:: .attachments/image-efc7d5cd-cf76-4b3d-83de-06e2a392d303.png


Funcionalidade
--------------

O cadastro de um Hook é baseado na seleção de um time, sendo ele um time folha ou não. No modal, deverá ser digitado o Nome para o novo cadastro. Nome e Time (selecionado anteriormente) são campos obrigatórios. Como descrito no botão "Salvar e gerar URL", será gerado URL que será de uso direto com o robô. 

.. figure:: .attachments/image-759b6218-b1de-410f-80cd-8b1b9e568594.png


Modal de detalhes
-----------------

Modal somente para visualização das chamadas a URL gerada para o Hook. 

.. figure:: .attachments/image-36a3a5c7-829c-458a-b180-785bcb11d1e8.png


Uso das URLs
------------
Para o uso correto das URLs geradas é necessário passar algumas informações:

**Mensagem direta**

Url de Hook com time folha: passar mensagem usando parâmetro **ticotico**. Exemplo: (GET) **.../hooks/[código-gerado]?ticotico=[Menasgem]**

Url de Hook sem time folha: passar nome do robô e mensagem usando parâmetro **robotname*** e     **ticotico** respectivamente. Exemplo: (GET) **.../hooks/[código-gerado]?robotname=[NomeRobo]&ticotico=[Menasgem]**

**Chamada de uma ação**

Ação sem parâmetros: passar código da ação (disponível da listagem de ações) usando parâmetro **actioncode**. Exemplo: **.../hooks/[código-gerado]?actioncode=[CodigoAção]**

Ação com parâmetros: há duas opções que pode atender a essa situação, parâmetros pela URL e parâmetros pelo corpo da requisição. Segue os exemplos: 

- pela URL (GET) **.../hooks/[código-gerado]?actioncode=[CodigoAção]&parametro=[Valor1]&parametro2=[Valor2]**			
            
- pelo corpo (POST) **.../hooks/[código-gerado]?actioncode=[CodigoAção]**			
	body{ Parâmetros }
			
**Paramêtros utilizados**

``actionCode`` (código de uma ação)

``robotName`` (nome do robô) 

``ticotico`` (mensagem a ser executada, não é necessário passar actionCode para esse caso)
                  