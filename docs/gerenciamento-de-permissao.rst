Gerenciamento de permissão
==========================

Objetivo
--------

Gerenciar permissões para usuários terem acesso a determinado time (de forma hierárquica). 

.. figure:: .attachments/listagem-gerenciamento-permissao.png

Funcionalidade
--------------

A edição/criação se dá pela seleção de um time a qual se quer conceder permissão. No modal, será digitado o Nome de Usuário e no qual, ao salvar, será verificado sua existência. Caso exista a pessoa, será concedido permissão, caso contrário será exibido mensagem informando que o usuário não existe.

Há dois tipos de permissão, que são: **Administrador** e **User**. Caso User sejá selecionada, o usuário só poderá fazer edições nas telas Action e Trigger do sistema diretamente para o time (e para os times no nível abaixo dele). No caso da permissão Administrador seja dada, além das permissões e User, o usuário terá permissão para adicionar/editar/excluir demais usuários para o time (e para os times no nível abaixo dele) em questão. 

.. figure:: .attachments/image-fbdf871f-8e4c-4bfc-9a30-aad5dacac2c6.png


