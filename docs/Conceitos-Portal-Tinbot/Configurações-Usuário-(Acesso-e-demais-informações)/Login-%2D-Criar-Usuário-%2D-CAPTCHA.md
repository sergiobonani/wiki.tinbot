Ao ser feito login e/ou criar usuário terá o Captcha responsável por limitar o uso no front-end.

![image.png](/.attachments/image-ff8114e5-ef9d-4bf8-9706-c6e62416c718.png) 

Tal implementação precisa que o site seja registrado no [Google Captcha](https://www.google.com/recaptcha/). Para cada site (homologação, produção e local).
