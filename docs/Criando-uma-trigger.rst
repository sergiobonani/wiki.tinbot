Criando uma trigger
-------------------

Aqui, iremos criar um evento que será responsável por disparar a ação, cadastrada anteriormente, ao Tinbot. 
Caso queira implementar outras de Trigger vá em :ref:`Triggers <Triggers>` para saber mais.

Antes de cadastrar uma Trigger, precisamos escolher um time e a regra é a mesm explicada no passo anterior. Então, dessa forma, após escolher o time onde deseja cadastrar a Trigger, iremos escolher o tipo de trigger que queremos.
Após escolher o tipo o botão adicionar irá ficar disponível para ser clicado.

.. figure:: .attachments/selecao-tipo-trigger.png

Para nosso primeiro cadastro iremos utilizar a Trigger por voz.
No modal de cadastro devemos preencher tanto o campo Condição como o campo ação. No campo condição, o que deve ser colocado é como nos comunicaremos com o Robô, escolheremos a condição "Bom dia". 
Já no campo Ação iremos colocar a ação cadastrada no passo anterior.

.. figure:: .attachments/criando-uma-trigger.png

Quando salvar o cadastro o Robô deverá alertar sobre a atualização de trigger. Mas isso só acontecerá se a trigger for criada diretamente para o time dele.