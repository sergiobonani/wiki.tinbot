Tinbot
======

.. toctree::
   :maxdepth: 2
   :caption: Primeiros passos
   
   criando-uma-conversa

.. toctree::
   :maxdepth: 2
   :caption: Outras Configurações
   
   configurando-robo
   configurando-webhook

.. toctree::
   :maxdepth: 2
   :caption: Conhecendo o Portal Tinbot
   
   Tinbots
   Actions
   Triggers
   variaveis-globais
   Conhecidos
   gerenciamento-de-hooks
   gerenciamento-de-permissao      
   Configuracoes de Times 
   Logs