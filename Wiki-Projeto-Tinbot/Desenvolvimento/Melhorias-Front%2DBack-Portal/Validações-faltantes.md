| Tela | Ação| Resultado | Ajustar |
| --- | --- | --- | --- |
| Login |  |  |  |
|| Logar sem informar usário e senha ou com dados errados | Mensagem em inglês | sim
|| Criar novo Usuário | Mensagem em inglês | sim
|| Criar novo usuário | Marcar campos em vermelho |
|| Criar novo usuário | Adicionar botão 'Cancelar' | sim
|| Criar Usuário/Loin | Campos em inglês | sim
| Pessoas
|| adicionar pessoa | Mensagem em inglês | sim
|| adicionar pessoa | Adicionando registro duplicado | sim
|| adicionar fotos | aceitando qualquer extensão | sim
|| editar/adicionar pessoa (adicionando foto) | ao adicionar fotos aleatórias sem rosto é gerado execption genérica (precisa tratar erro e exibir mensagem de foto inválida/sem rosto) | sim
| Tinbot 
||| nomes em inglês | sim
| Ações
|| Cadastro ticotico/js | deixando salvar sem informção | sim
|| Cadastro ticotico/js | deixando enviar para teste sem informação descrita | sim
| Triggers
|| Cadastrar uma trigger | Mensagens de valadções aparecendo em dois 'balões' | sim
|| Cadastrar uma trigger | autocomplete com seleção ruim | sim
| Sobre
|| Sem utilidade
| Configurações
|| confuso
| Home
|| Sem informação