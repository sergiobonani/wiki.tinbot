###[Login](https://beta.tinbot.club/login)


| | Campo| Obrigatório| Tipo/Tamanho |
| --- | --- | --- | --- |
|| Usuário | sim | ??
|| Senha | sim | ??
|| Nome Completo | ??
|| Email| sim | ??
|| Telefone | sim | ??

###[Pessoas](https://beta.tinbot.club/guys)

|| Campo| Obrigatório| Tipo/Tamanho |
| --- | --- | --- | --- |
|| Nome | sim | ??
|| Email | sim | ??
|| Fotos | não | ??

###[Ação](https://beta.tinbot.club/actions)

| | Campo| Obrigatório| Tipo/Tamanho |
| --- | --- | --- | --- |
|| Nome | ?? | ??
|| Descrção | ?? | ??
|| Ação | ?? | ??

###[Trigger](https://beta.tinbot.club/triggers)

| Tela | Campo| Obrigatório| Tipo/Tamanho |
| --- | --- | --- | --- |
| Trigger
|| Ativo | sim | bool
|| Condição | sim | ??
|| Ação | sim | ??

###[Configurações - Tinbot ](https://beta.tinbot.club/tinbots)

| Tela | Campo| Obrigatório| Tipo/Tamanho |
| --- | --- | --- | --- |
| Configuração
|| Ação Especial | não | ??
|| Ação Básica | não | ??
