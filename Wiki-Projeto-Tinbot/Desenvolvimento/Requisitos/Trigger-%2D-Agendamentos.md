Será utilizado a tela de Triggers para que possa ser feito a alteração. Será adicionado um botão para adição de agendamentos ao lado dos demais botões de JS e voz.

####1 Grid de Listagem

A grid continuará exibindo as informações de **Tipo**, **Ativo**, **Condição** e **Ação**. Será adicionado a coluna **Detalhes**, onde a mesma ficará responsável por exibir informações de Triggers do tipo JS e Agendamento.

Para JS, a coluna **Detalhes** deverá exibir as informações dos campos ```Tempo Mínimo entre Execuções``` e ```Intervalo de teste```. Como forma de exemplo:
.. | Ação | Detalhes | ..
--|--|--|--|
|| ação 1 | **Tempo mínimo: 2h - Intervalo: 30min** 

Para Agendamento, a coluna **Detalhes** deverá exibir as informações dos campos ```Data Início da execução```, ```Data término da execução``` e ```Horário da Execução```. Como forma de exemplo:
.. | Ação | Detalhes | ..
--|--|--|--|
|| ação 3 | **Início: 20/09/2018 - Término: 20/12/2018 - Horário: 08:00**
|| ação 4 | **Início: 19/10/2018 - Término: indefinido - Horário: 17:30**

Coluna em azul é a alteração que deverá ocorrer na grid de listagem:

![image.png](/.attachments/image-e22873f7-8d86-4bcc-ab70-dbaabfb177a4.png)

####2 Tela de Inserção/Edição

Campo | Obrigatorio | Tipo | Descrição
-- | -- | -- | -- |
Nome Trigger | sim | string | Nome da trigger que está sendo cadastrada
Ativo | sim | boolean | Responsável por sinalizar que o agendamento estará ativo para uso ou não
Horário de Execução | sim | time | Horário previsto para ser executado a trigger. Em caso de Horário menor que o horário atual, será executado no próximo dia.
Repetir Execução | não | boolean | Campo responsável por afirmar ou não que haverá repetição do agendamento.
Dias da semana | não | lista | Campos de dias somente disponível para seleção se o campo **Repetir Execução** estiver selecionado. Será composto por 7 botões compondo os dias da semana, ou seja, (D)(S)(T)(Q)(Q)(S)(S). Todos botões devem vir selecionados por default. Caso seja salvo sem alteração, será considerado todos os dias.
Data Início | não | Datetime | Campo habilitado somente se o campo **Repetir Execução** estiver selecionado. Se for selecionado data inferior a data atual, será considerado a data atual para o cadastro. Campo nunca deve conter data superior a **Data Término**
Data Término | Não | Datetime | Campo habilitado somente se o campo **Repetir Execução** estiver selecionado. O campo terá limitação de 1 ano, datas superiores a isso deverão retornar mensagem de data fora do intervalo permitido. Caso não seja selecionado valor no cadastro, esse campo ficará como **indeterminado**, respeitando o limite maximo de 1 ano.
Ação | sim | string | Serão exibidos ações relacionadas ao time em que está sendo inserido o agendamento para trigger. Caso já possua um cadastro para tal ação selecionada, será exibida mensagem de duplicidade de campo

####Outras informações

Para que o registro permaneça ativo é necessário respeitar a data término da execução. Caso a data já tenha passado o registro precisará ser desativado.
Quando o registro estiver desativado, ele poderá ser reativado pela edição. Nesse caso, ao selecionar o ativar os campos de datas deverão ser limpos. Caso o registro seja salvo sem informação de data o mesmo permanece com tempo indeterminado.

No caso das triggers por agendamento cadastradas mas que o Robô não tenha baixado tais informações ou não tenha executado, se for agendamento unico, o mesmo não será executado¹. Se for repetição, o mesmo será executado na proxima data prevista².

1. No caso do agendamento único, aquele que não tem data mas somente horário, não será executado pelo robô ao ligar/baixar as informações do portal. Ou seja, no caso do horário seja as 08:00 e o robô tenha baixado/ligado as 09:00 essa trigger deverá ser desconsiderada.

2. No caso do agendamento com repetição, se a hora de execução já tenha passado só será executado no próximo dia cadastrado. Independente se for em dias alternados.

Para o Robô ficará a responsabilidade de 'distanciar' as execuções das trigger de JS e agendamento caso as duas coincidam no mesmo horário. Será executado primeiramente JS e após 5 minutos a trigger agendada.

Além do cadastro de agendamento, o Portal terá a responsabilidade de inativar agendamentos que tenham a data de execução 'vencida'. A regra que deverá valer para intativar registros será a data atual menos 12 horas³. Com isso, baseado na **data término**, será verificado e inativado.

3. A opção de escolha para subtrair por 12 foi baseada no fuso horário das localizações mais extremas a partir da data/hora local. 

A tela de inserção/edição deverá ser um modal com os itens descrito na grid acima. Coforme inserido ou alterado, deverá retornar para a tela de listagem com as informações efetuadas.

![image.png](/.attachments/image-49a7bfe2-3318-49ff-894f-bf55bd32f138.png)

