Hooks

_O usuário do sistema, enquanto administrador, será capaz de criar/remover links com base em times._ 

Criar tela no sistema para cadastro dos links que serão usados externamente e deverá estar no menu Gerenciamento.

**Gerenciamento >> Geração de Hooks**

Deverá ter dropdown de seleção de times, um campo livre de digitação para filtrar grid e o botão **Adicionar**. 

Para o botão, só será ativo quando dropdown de times estiver selecionado. Dessa forma, quando clicado, irá abrir um modal para inserir novo link.

Os campos que serão exibidos serão **Nome (Obrigatório)** e **Time**. Sendo que time será somente leitura, contendo o nome do time selecionado anteriormente.

Para a listagem os campos que deverão ser exibidos no grid são **Nome**, **Time**, **Url** e as opções de **detalhes** e **excluir**.

![image.png](/.attachments/image-fe7f00eb-b6b0-4b66-9f84-6de68a600cae.png)
_Tela de listagem_

![image.png](/.attachments/image-dc70f693-450f-4e2a-8267-d869a450bc65.png)
_Modal de inserção de links_

A opção Detalhes deverá exibir em um modal todos os acessos realizados através da URL, seus parametros, data de acesso e o retorno da Api. Essas informações deverão ser cadastradas na parte pertinente ao acesso as chamadas dessa URL, que será comentada mais abaixo.

![image.png](/.attachments/image-eb19e04a-1326-4f6c-bd35-7304f0177ecd.png)
_modal de detalhes_

----

Trigger

Na criação/edição de trigger, considerando a possibiliadade de actions que recebam parâmetros, será possível cadastrar chaves e valores que serão usados pelas actions. Tal valores deverão ser cadastrados conforme item criado em Action.

Deverá haver, abaixo dos campos existentes, um campo chamado **Parâmetros** onde ao clicar em "+" ser´exibido dois campos: **Nome** e **Valor**.

A alteração deve contemplar todos os tipos de Trigger.

Alterar campo Ações para exibir link direto a tela de edição do mesmo. Objetivo é ter mais agilidade para acessar Ações selecionadas nas Triggers.

----

Chamadas:

URL gerada para um time, passando número de série e mensagem
**.../hooks/[código-gerado]/Tinbot1000/{{Menasgem}}**
Deverá retornar informação de sucesso ou erro com descrição parcial do erro

URL gerada para um time, passando número de série, action e parâmetros
**.../hooks/[código-gerado]/Tinbot1000/action={{cód action}}&parametro={{valor}}&parametro2={{valor2}}**
Deverá retornar informação de sucesso ou erro com descrição parcial do erro

URL gerada para um time, passando número de série e action
**.../hooks/[código-gerado]/Tinbot1000/action={{cód action}}**
Deverá retornar informação de sucesso ou erro com descrição parcial do erro

URL gerada para um time, passando número de série, action e corpo com parametros
**.../hooks/[código-gerado]/Tinbot1000/action={{cód action}}**
**_body{
  Parametros
}_**
Deverá retornar informação de sucesso ou erro com descrição parcial do erro

Para as chamadas deverão ser validados:
* **Código gerado**: caso o código gerado não corresponda a nenhum cadastrado deverá retornar mensagem de erro informando a inconsistência 
* **Número de série**: caso valor passado não corresponda a nenhum robô do time relacionado ao código gerado deverá retornar mensagem de erro
* **Código da action**: caso esse valor seja passado, validar a existência do mesmo e se está relacionado ao time da url, se o mesmo não tiver relação retornar mensagem de erro
* **Status Robô**: verificar se o Robô encontra-se ativo, caso não esteja retornar menasgem de erro.

Após validações, enviar ao Robô as informações correspondentes a ação selecionada e os paramêtros passados (se exister). Informações devem ser enviados em formato json.

Após validações e/ou envio ao Robô das informações, deverá salvar um log de hooks as informações de URL, parametros, data de acesso e o status dela (sendo de erro ou ok) para posteriormente ser consultado o acesso da mesma.

-----

Actions

_Nas ações JS será possivel criar e receber parâmetros para sua execução._
