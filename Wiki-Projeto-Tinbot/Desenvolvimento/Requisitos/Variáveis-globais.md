_Deverá ter opção para cadastro de variáveis globais para utilização de valores e mais de uma action. Tal cadastro deverá ser feito em uma tela tendo a opção de criar/editar/remover._

Criar Cadastro de Variáveis para serem usados em todas as Actions cadastradas.
O caminho deverá ser **>> Variáveis Globais **

A tela de listagem deverá exibir os Parametros criados e seus valores. Deverá conter um dropdown de Times, um campo de pesquisa e os botões  **Adicionar**, **Salvar** e **Cancelar**.

O botão Adicionar só deverá estar ativo caso haja Time selecionado no dropdown.
Os botões Salvar e Cancelar só deverão estar ativos caso seja adicionado novos valores.

![image.png](/.attachments/image-0954170e-7dfb-411d-a88d-dc60f7cc320e.png)

Ao salvar deverá validar:
* Campo Time: validar a existência do time, caso o time informado não exista deverá retornar mensagem de erro
* Campo Valor: deverá possuir 1 ou mais caracteres, caso esteja diferente deverá retornar mensagem de erro
* Campos Chave: Deverá possuir 3 ou mais caracteres, caso esteja diferente deverá retornar mensagem de erro. Verificar se não há registro já cadastrado com o mesmo nome para o mesmo time

Após a criação dessa tela, criar método para que o Robô consuma as informações cadastradas afim de obter e armazenar variáveis com o objetivo de utilizar posteriormente nas actions cadastradas. Deverá enviar as variáveis cadastradas ligadas diretamente ao time do Robô e as variáveis cadastradas dos superiores do time.

