####O que vai ser feito
Será implementado as configurações para o robô e/ou superiores no formato XML.
Descontinuando o formato utilizado atualmente, que é o json.

####Por que isso será feito?
Será implementado esse novo formato de configuração visando o melhor uso, entendimento e 
facilitando a adição de comandos e ações pré configuradas para os Robos

####Onde vai ser feito?
Será feito nas Configurações de Tinbot/Time

####Qual valor para o cliente?
Não será necessário duplicar mais configurações, em caso de dois ou mais Robôs do mesmo cliente. Configuração hierarquica, podendo deixar configurações especificas para times/setores
e consequentemente para os robos. Não haverá dependencia de uma pessoa para atualizar cada time folha, visto que os parametros e configurações ajudarão nos novos parametros adicionados no robô. E uma base para futuramente ter configurações intercambiaveis, como por exemplo o robô poder assumir vários modos e as configurações se destinarem especificamente para determinado modo.

####Quais impactos no sistema? 
Alteração de linguagem JSON para XML. Atualizações feitas em Robôs perdendo o uso de Json para XML,ou seja, haverá uma necessidade de acompanhar o cliente para subistituição do JSON para o XML. 


####Alterações e implementações no sistema

- Cadastro de XML por times
   - Editável somente no cadastro do próprio time
   - Exibir Colunas com: 
         1 - XML combinado com superiores do time que está sendo editado 
         2 - Outra para editar/criar XML para Time atual
         3 - Outra para mesclar XMLs combinados com superiores e com XML do time atual. Para essa terceira coluna, deverá possuir botão :arrows_counterclockwise:  para exibir mesclagem. Caso a tela seja acessada e já tenha configurações para o time atual e superiores, essa coluna deverá vir preenchida.
                3.1 - Botão preview deverá vir desabilitado caso não haja informações na coluna Previous ou Current
                3.2 - Em caso de erro, ao acionar preview, deverá notificar usuário (verificar a possibilidade de exibir onde está o erro dentro do XML).
   - Colunas dos itens 1 e 3 não devem ser salvos em banco e sim somente criados para exibição em tela
   - Deverá validar se XML inserido está correto, sem erros.

Ao salvar uma alteração o robô deve ser avisado somente caso a alteração seja a nivel de time folha, alterações feitas nos times superiores não deverão ser notificadas, nesse caso as alterações serão atualizadas no robô quando o mesmo for reiniciado/ligado.
 
O cadastro de XML deverá ser numa aba separada da estrutura atual existente e essa ser aberta como principal. 

Segue esboço de como deve ficar a tela para o usuário final
![image.png](/.attachments/image-6332482d-5e43-4f03-895e-88eb6d1f88cb.png)

- Enviar lista de XML para robô
   - Deverá prover caminho para o robô buscar lista de XML, sendo esses o XML do time folha e seus superiores
   - Cada XML deverá vir com sua 'origin' marcada, ou seja, campos sinalizados de qual time pertence
   - A lista de XML deverá ser enviada em ordem Superior para time folha, exemplo: Factory, Evoa, Tinbot10


#####Estimativas macro

Solução técnica: 3 horas
Testes Unitários: 5 horas
Business/Api: 15 horas
Tela: 11 horas
Publicação e testes: 5 horas



   