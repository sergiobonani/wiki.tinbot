##Testes Unitários

Tela | Classe | Métodos
-- | -- | -- 
Permissão | PermissionBusiness | Insert/Update/Delete
Espelho | MirrorBusiness | Recalculate Person/Team/All
Ações | ActionScriptBusiness | Insert/Update/Delete
Triggers | TriggerBusiness | Insert/Update/Delete
Time | TeamBusiness | Insert/Update/Delete
Tinbot | TinbotBusiness | Insert/Update/Delete

Atividade | Descrição | otimista | assertiva | pessimista
-- | -- | -- | -- | -- |
Massa de dados | Inserir lista de **times** com descendencia e avulso | 1 | 2 | 3
Massa de dados | Inserir lista de **permissões** | 1 | 1,5 | 2
Moq Persistência banco | Pesquisar e aplicar moq para requisições que envolvem banco | 2 | 3 | 5
Testes | Efetuar testes métodos das classes descritas acima. Utilizar XUnit e FluentAssertion para montar os testes. Os testes deverão ser feitos Insert/Update/Delete Ok e validações OK/NOK | 20 | 30 (2h/teste) | 40
|| Total | 24 |36,5 | 50

##SonarQube

Atividade | Descrição | otimista | assertiva | pessimista
-- | -- | -- | -- | -- |
Instalar Sonar | [Instalar](https://blogs.msdn.microsoft.com/devops/2015/09/28/quickstart-analyzing-net-projects-with-sonarqube-msbuild-or-visual-studio-online-and-third-party-analyzers-stylecop-resharper/) Sonar no servidor. 
Configurar Sonar | Configurar regras, ambiente, integrar com build..
| | Total  | 12| 16| 20

##Configuração de ambiente na Amazon

Atividade | Descrição | otimista | assertiva | pessimista
-- | -- | -- | -- | -- |
Configuração IIS | Configurar Sites Beta e Prod, deixando funcionando localmente
Configurar HTTPS | Instalar CertifySSL, configurar bindings no IIS
Fazer backup da base | Efetuar backup dos ambiente do servidor antig e passar para o novo
Configurar Backup | Instalar SQL Backup Master e configurar para ambientes
Configurar Domonios | Apontar dominios para novo ambiente
| | Total | 12 | 16 | 20
