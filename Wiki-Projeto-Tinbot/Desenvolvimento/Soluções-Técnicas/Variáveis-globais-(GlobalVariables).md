Back-end

* Criar ```GlobalVariablesController```
   * Criar endpoints de listagem, inserção e exclusão de itens
* Criar ```GlobalVariablesBusiness```
   * Criar método de inserção. Fazer validações pertinente ao método
   * Criar métodos de exclusão e listagem
   * Criar método ```GetGlobalVariablesForRobot```, que receba Nome do Robô e retorne todas as variáveis do time desse robô e de seus superiores.
* Criar testes unitários
* Alterar RobotController e inserir método que consuma ```GetGlobalVariablesForRobot```. 

Front-End

* Alterar menu e inserir item para acesso a tela de listagem de Variáveis globais
* Criar componente (html, css e ts) ```GlobalVariables```
     * Criar grid de listagem e inserção de valores
