#####Front (14 hrs)
- Alterar listagem de trigger 
   - Alterar botões de inserção de trigger e utilizar button-touggle (fazer regra para exibir modal corretamente) e um único adicionar
   - Alterar grid e inserir coluna **detalhes**
- Criar modal para edição/inserção de agendamento
  - Criar components para modal

#####Back (36 hrs)
- Alterar Entidade Trigger e inserir novos campos
  - Mapear e criar migration
- Alterar controller Trigger
   - Alterar retorno da listagem para contemplar campo de Detalhes
   - Alterar métodos de inserção e edição para contemplar tipo agendamento (utlizar flag enum para dias da semana)
   - Efetuar validações novas
- Criar testes unitários
- Criar 'scheduler' para que seja rodado uma vez ao dia no objetivo de desativar registros, caso o mesmo tenha data término 'vencida'