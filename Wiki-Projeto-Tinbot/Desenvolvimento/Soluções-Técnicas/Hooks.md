Back

* Criar mapeamento e migrations para Hooks
* Criar mapeamento e migrations para HooksLogDetails
* Criar ```WebHooksController```
  * Criar método de inserção de novos hooks (recebendo Nome e TimeId e retornando URL)
  * Criar métodos de exclusão e listagem
  * Criar método ```GetHookLogDetails``` para retornar informações do modal de detalhes de acesso  
* Criar ```HooksController``` (Sem autenticação)
   * Criar métodos GET/POST para consumo das URL geradas
* Criar ```HooksBusiness```
  * Criar método de inserção
     * Validar obrigatoriedades
  * Criar métodos de exclusão e listagem
  * Criar método para obter detalhes
     * Obter informações de ```HookLogDetails```
  * Criar método para inserção de detalhes ```InsertHookLogDetails```
  * Criar método ```SendCommandForRobot```. Será consumido pelos metodos GET/POST de acesso externo (URL geradas) do controller
     * Validar obrigatoriedades
     * Enviar comando para o robô (utilizar hub e modelo já existente como exemplo)
     * Inserir HookLogDetails
* Criar mapeamento e migrations para ```TriggerVariables```
* Alterar viewModels e entidade de trigger para considerar as variáveis (TriggerVariables) para cadastro
* Alterar TriggerBusiness 
   * Validar inserção de variáveis (TriggerVariables)
   * Inserir variáveis
* **Criar TESTES para novas implementações**

Front

* Alterar menu e inserir opção _Geração de Hooks_ dentro de _Gerenciamento_ 
* Criar componente (html, css e ts) Hooks
   * Criar grid de listagem
   * Criar modal de inserção de novos Hooks
   * Criar modal de detalhes de acessos
        * Deixar disponivel link para acesso a Ação através do código exibido
* Alterar modais de inserção/edição de Triggers
   * Exibir a opção de adição de parâmetros
        * Criar componente e utilizar o mesmo paratodos os tipos de Trigger
   * Alterar autocomplete de Ação para exibir link direto a tela de edição da ação selecionada
* Alterar listagem de trigger para, na coluna ação, exibir link direto a tela de edição da ação selecionada

