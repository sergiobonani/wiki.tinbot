Ordem | Descrição | Classe Criada/Afetada | Status
| -- | -- | -- | -- |
|1 | Criar Entidade **Espelho** { TimeId, IsAdmin, PersonId } | Mirror | :heavy_check_mark:
|1.1 | Gerar Migration | | :heavy_check_mark:
|2 | Criar Business para Espelho | MirrorBusiness | :heavy_check_mark:
|2.1 | Criar método ```RecalcularParaTime(timeId)``` | MirrorBusiness| :heavy_check_mark:
|2.1.1 | Deverá deletar espelhos baseados no ```TimeId``` passado| |:heavy_check_mark:
|2.1.2 | Obter ```ParentId``` consultando tabela de ```Times``` baseado no ```TimeId``` passado. Caso o time não possua ```ParentId``` o processo não termina.|| :heavy_check_mark:
|2.1.3 | Obter ```Espelhos``` cadastrados baseados no ```ParentId``` | | :heavy_check_mark:
|2.1.4 | Inserir Espelhos baseados nos campos ```TimeId```(2.1), Espelhos.IsAdmin(2.1.3) e Espelhos.PersonId(2.1.3) | | :heavy_check_mark:
|2.2 | Criar método ```RecalcularParaPessoa(PersonId)``` | MirrorBusiness|  :heavy_check_mark:
|2.2.1 | Deverá deletar espelhos baseados no ```PersonId``` passado| | :heavy_check_mark:
|2.2.2 | Em ```UserContextData```, Alterar/Criar método para retornar ```Permissões``` baseadas no ```PersonId``` passado | |  :heavy_check_mark:
|2.2.3 | Obter ```Times``` com permissão (IsAdmin) direta/indireta através do método ajustado em 2.2.2| | :heavy_check_mark:
|2.2.3.1 | Inserir ```Espelhos``` baseados nos campos ```PersonId```(2.2), Times.Id(2.2.3) e IsAdmin(true) | |  :heavy_check_mark:
|2.2.4 | Obter ```Times``` com permissão (!IsAdmin) direta/indireta através do método ajustado em 2.2.2| |  :heavy_check_mark:
|2.2.4.1 | Inserir ```Espelhos``` baseados nos campos ```PersonId```(2.2), Times.Id(2.2.4) e IsAdmin(false) | |  :heavy_check_mark:
|3 | Criar método ```RecalcularTudo``` (ver para utilizar no startup) | MirrorBusiness | :heavy_check_mark:
|3.1 | Deletar todos os espelhos ||:heavy_check_mark:
|3.2 | Obter todos ```PersonId``` a partir de ```Permissão``` usando Distinct||:heavy_check_mark:
|3.3 | Executar método ```RecalcularParaPessoa(PersonId)```(2.2) para lista de ```PersonId``` obtido ||:heavy_check_mark:
|- | - |- |
|4 | Criar métodos de consulta baseado nas ```Permissões```. Utilizar lógica subistituindo o uso de ```Permissions``` pela tabela ```Espelho```|| :heavy_check_mark:
|5 | Verificar uso do UserContextData nas classes/métodos e substituir pelos novos métodos criado no item 4| TimeController, TeamBusiness, ActionScriptBussines, RobotDataController e ActionScriptController |  :heavy_check_mark:
|5 | Simular testes com o uso do modelo antigo e novo e verificar o tempo gasto. Avaliando dessa forma se o novo modelo melhora a performance | |  :hourglass:



