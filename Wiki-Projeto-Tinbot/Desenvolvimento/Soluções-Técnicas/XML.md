###Back

####Cadastro
- Criar casos de testes
   - para o cadastro do XML
   - para mesclar XMLs
   - para receber lista de XMLs
- Alterar entidade Teams e inserir o campo XMLConfiguration do tipo string
- Criar Migration para inserir o campo XMLConfiguration na tabela. Dessa forma ser possível persistir e consumir valores salvos na configuração de XML
- Alterar RobotDataViewModel e criar o campo XMLCurrent, para ser possivel receber as alterações do campo no viewModel e utilizá-lo no Update em RobotDataController
   - Inserir também os campos XMLPrevious e XMLPreview. Tais campos, junto com XMLCurrent, serão usados posteriormente para enviar as configurações de XML para o portal
- Criar o método UpdateXMLSettings, em TeamBusiness, para que seja possível persistir as novas configurações de XML para o time
    - Inserir o campo XMLConfiguration e TeamId como parametros de entrada 
    - Validar a existência do Time e validar XML (verificar se o mesmo não contém erros), caso não atenda alguma das validações retornar mensagem invalidando a persistência e com motivo 
    - Persistir o XMLConfiguration para o time
- Alterar método Update, em RobotDataController, para consumir o método UpdateXMLSettings afim de salvar alterações feitas nas configurações por XML
   - Verificar se há informação no campo XML e TeamId, caso sim, consumir método UpdateXMLSettings da classe TeamBusiness
- Em TeamBusiness, alterar o método GetRobotDataByLeaf para obter o XMLConfiguration do time e de seus superiores para serem retornados junto aos demais campos e serem consumidos pelo portal
   - Criar método privado para obter os XMLConfiguration dos times superiores
   - Ao obter XMLConfiguration dos superiores, utilizar método ApplyOrigin da classe disponível no tinbot.shared(nuget) para demarcar cada XMLConfiguration ao seu time de criação
   - Após os itens anteriores, mesclar os XMLConfigurations dos Times superiores para utilizá-los como XMLPrevious. E mesclar os XMLConfigurations dos Times superiores com o XMLConfiguration do time atual para utilizá-lo como XMLPreview (no XMLPreview é necessário utilizar o método ApplyOrigin também)
- Criar viewmodel UpdatePreviewViewModel com os campos XMLPrevious, XMLCurrent e TeamCurrent. Esse viewmodel será utilizado para o item abaixo
- Em TeamBusiness, criar método UpdatePreview e deverá receber UpdatePreviewViewModel. Esse método será responsável por mesclar XMLPrevious e XMLCurrent para ser exibido no portal
    - Utilizar o método ApplyOrigin no XMLCurrent antes de mescá-lo com XMLPrevious
- Em RobotDataController, criar o endpoint UpdatePreview que receba como parâmetro UpdatePreviewViewModel. Esse endpoint será responsável por retornar ao Front os XMLs dos tipos superiores e atual mesclados e o mesmo deverá utilizar o método UdatePreview, criado anteriormente


####Envio ao robô
- No RobotSyncBusiness, criar método GetXMLSettings que será responsável por obter os XMLs configurations do Time folha e de seus superiores. Deverá ter como método de entrada o SerialNumber do robô
   - A partir do SerialNumber, obter o robô e posteriormente seu time para obter o XMLConfiguration dele e também obter XMLConfigurations dos times superiores
   - Aplicar para cada XMLConfiguration o ApplyOrigin para que o robô possa identificar de qual time pertence o XML
   - Ordernar a lista de XMLConfiguration por ordem de superior para time folha 
- No RobotController, criar endpoint GetXMLSettings com o parâmetro de entrada SerialNumber. Esse endpoint será consumid pelo Robô para obter os XMLs de configuração do time folha e de seus superiores
   - Consumir o método GetXMLConfigurations criado anteriormente no RobotSyncBusiness

###Front

- Alterar RobotSettings 
   - Criar Abas para exibir Json e XML separados (componente mat-tab do material angular)
   - Na aba Json manter estrutura atual
   - Na aba XML inserir 3 colunas (Previous, Current e Preview)
         - Na coluna Preview inserir botão de atualizar preview, onde o mesmo enviará as informações para o endpoint UpdatePreview para mesclar as duas outras colunas e receberá o retorno com o valor atualizado
