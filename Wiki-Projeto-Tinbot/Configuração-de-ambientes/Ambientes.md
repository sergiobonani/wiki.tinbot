## Homologação

| Aplicação | URL | Branch | Objetivo |
| --- | --- | --- | --- |
| Portal Tinbot | <https://beta.tinbot.club> | PortalTinbot/Master | Homologação de versão atual em desenvolvimento |
| Api Tinbot | <https://betaapi.tinbot.club> | PortalTinbot/Master| Caminho para acesso direto a Api usada pelo portal |
| Hub Tinbot | <https://betahub.tinbot.club> | PortalTinbot/Master|  |
| APP Tinbot Settings | <https://{NumeroSerieTinbot}:8800>  |  | Acesso as configurações utilizadas pelo robô |

## Produção

| Aplicação | URL | Branch | Objetivo |
| --- | --- | --- | --- |
| Portal Tinbot | <https://tinbot.club> <https://prod.tinbot.club> | PortalTinbot/Master | Ambiente de produção usado pelo cliente |
| Api Tinbot | <https://api.tinbot.club> <https://prodapi.tinbot.club> | PortalTinbot/Master|  Caminho para acesso direto a Api usada pelo portal  |
| Hub Tinbot | <https://hub.tinbot.club> <https://prodhub.tinbot.club> | PortalTinbot/Master|  |
| APP Tinbot Settings | <https://{NumeroSerieTinbot}:8800> |  | Acesso as configurações utilizadas pelo robô |

