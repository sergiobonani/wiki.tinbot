##Configuração a partir do VSTS

###1º Build dos ambientes 

##### Ambiente Homologação
[Link](https://akus.visualstudio.com/TinBot/_build/index?context=mine&path=%5C&definitionId=1&_a=completed) para build da homologação.

[![Build status](https://akus.visualstudio.com/TinBot/_apis/build/status/Portal-CI)](https://akus.visualstudio.com/TinBot/_build/latest?definitionId=1)

A branch utilizada para build é a `Homologacao`

##### Ambiente Produção
[Link](https://akus.visualstudio.com/TinBot/_build/index?context=mine&path=%5C&definitionId=8&_a=completed) para build de produção.

[![Build status](https://akus.visualstudio.com/TinBot/_apis/build/status/Portal-Production-CI)](https://akus.visualstudio.com/TinBot/_build/latest?definitionId=8)

A branch utilizada para build é a `Master`. Tal branch só deve conter check-in originados a partir de da Branch Homologacao, dessa forma para manter a estabilidade do sistema de produção e não afetar nenhuma funcionalidade já utilizada por clientes.

###2º Publicação dos ambientes

##### Ambiente Homologação