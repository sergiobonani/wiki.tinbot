###Objetivo
Listar todas as Famílias de todos os Times que o usuário logado tem permissão.
Será assumido no momento apenas 2 Níveis, sendo Time Raiz, Time Comum, Time Folha, porém a base de dados e lógica deve ser pensada para infinitors níveis posteriormente.

![image.png](/.attachments/image-362e8ba3-ced8-4b06-b6f7-6f4630abd504.png)

###Funcionalidades
A listagem seguirá a ordem da hierarquia, seguindo a sequência abaixo:

1) Um Time raiz das Famílias que dos Times que o usuário tem permissão;
2) Todos os Times Folha que são Filhos Diretos de 1);
3) Um Time Comum que seja Filho de 1);
4) Todos os Times Folha que sejam filhos de 3);
- Enquanto houverem mais Times Folha filhos de 1) voltar para 3)
- Enquanto houverem mais Times Raiz disponíveis para esse usuário voltar para1)

Os botões de função serão vísiveis apenas para os times quais o usuário tem acesso.
Os botões de função tem as seguintes funcionalidades:

```Configuração```
![image.png](/.attachments/image-466bee20-bf35-4b70-b344-772deacada4b.png) Direciona para a tela de edição configurações do Time Folha

![image.png](/.attachments/image-3b3b9514-337d-417e-bbbf-9fbe0c6e869f.png)

```Ações```

![image.png](/.attachments/image-197f792b-6584-448d-a1b4-212d28983e6c.png) Direciona para a tela de ações do Time Folha

![image.png](/.attachments/image-cb75f4e4-8bc4-4528-9779-253a0f09e128.png)

```Gatilhos```

![image.png](/.attachments/image-a5f64af4-66c4-4352-9582-c65621b68ff1.png) Direciona para a tela de gatilhos do time folha

![image.png](/.attachments/image-58a5045b-ae62-4017-87c6-c259ef4437e9.png)

```TicoTico```

![image.png](/.attachments/image-8e4d09e1-059d-41dd-b9eb-a3d826eb079a.png) Abre o modal para a execução de um Ticotico manualmente - desabilitado se o Robo associado estiver Desconectado

![image.png](/.attachments/image-ea067ca4-9cce-4640-b30d-4077fe7fc6e9.png)

---
---

Times folha terão todos os botões de função
Já times comuns terão apenas o botão de ações
![times.PNG](/.attachments/times-7f6d7947-0d68-47b1-9dfe-13603bca148c.PNG)