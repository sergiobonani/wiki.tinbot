####Objetivo
Criar, editar e excluir permissões para outras pessoas terem acesso a determinado time e, caso tenha, sua descendencia. 
![image.png](/.attachments/image-79ac3c1e-11d2-4e06-b2b9-eb9d01975a7e.png)

####Funcionalidade
A edição/criação se dá pela seleção de um time a qual se quer conceder permissão. No modal, será digitado o Nome de Usuário e no qual, ao salvar, será verificado sua existência. Caso exista a pessoa, será concedido permissão, caso contrário será exibido mensagem informando que o usuário não existe.

Há dois tipos de permissão, que são: **Administrador** e **User**. Caso User sejá selecionada, o usuário só poderá fazer edições nas telas Action e Trigger do sistema diretamente para o time (e sua descendência). No caso da permissão Administrador seja dada, além das permissões e User, o usuário terá permissão para adicionar/editar/excluir demais usuários para o time (e sua descendência) em questão. 

![image.png](/.attachments/image-fbdf871f-8e4c-4bfc-9a30-aad5dacac2c6.png)

