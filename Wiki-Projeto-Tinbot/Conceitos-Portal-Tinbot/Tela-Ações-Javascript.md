####Objetivo
Criar, editar ou executar uma ação no Tinbot através de Javascript
![image.png](/.attachments/image-2a90a206-740d-4ad5-a4f8-ce3809d3555e.png)

####Funcionalidade

* Edição de código com editor "Moncao Editor" ou smilar
![image.png](/.attachments/image-8cae3f3b-6fb4-4114-b64c-2694e943820e.png)

* Lista de ajuda com exemplos e instruções
** Cada botão abre um modal com o guia em questão
** O modal vai ter o mesmo componente de edição de código 
** Cada item tem Título e Código
** Os itens serão fornecidos em um JSON, que pode ser carregado junto com a tela
![image.png](/.attachments/image-6760fc39-8ac4-4411-977a-9b803e301969.png)

* Listagem de Times Folha para execução do código
**Serão listados apenas os Times Folha que estão "online" e que o usuário tem Acesso
**O Botão "play" executa o código no Folha selecionado
![image.png](/.attachments/image-e0602e11-3e00-4890-b40a-062fb3ce713f.png)