Atualmente dentro do ```VSTS Tinbot``` há a separação de três(3) repositórios. Sendo eles:

* Tinbot: Repositório atual (que será descontinuado) contendo o aplicativo do Tinbot e também o portal.  Sendo a ```TinBot2EletronicValidation.sln``` para o aplicativo e a pasta Portal com a ```PortalTinbot.sln``` para o portal.

* AppTinbot: Novo repositório para o aplicativo, porém ainda não sendo utilizado.

* PortalTinbot: Novo repositório para o Portal e já em uso para homologação, posteriormente será adotado como portal padrão para o ambiente de produção também.