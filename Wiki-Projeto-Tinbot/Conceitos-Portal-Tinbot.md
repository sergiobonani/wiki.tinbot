###Usuário
A Pessoa com acesso ao Portal tinbot, possui um nome de usuário, e-mail, senha e é relacionado a uma Pessoa no banco de dados

- Pessoa: Ligada diretamente com o usuário, representa um usuário, tem Nome ou outros dados

###Robô
Equipamento de hardaware que diz respeito à 1 unidade do Tinbot

###Time
Grupo hierárquico de Tinbots, um Time pode possuir vários times-filhos

- Time Raiz: Um time que não possui um Time superior
- Time Folha: Um time que representa uma unidade do Robô, nunca possui filhos
- Time Comum: Um time intermediário, não é um Time Raiz nem um Time folha
- Filho Direto: Todos os Times logo abaixo do time em questão na hierarquia
- FIlho Indireto: Todos os Times logo abaixo do time em questão e também todos os times abaixo desse

###Permissão
Ligação entre um usuário (através da classe Pessoa) e um Time que indica que esse usuário pode gerenciar esse time

- Acesso:Permissão indireta através da hierarquia - um usuário com permissão para um Time terá Acesso à esse Time e a todos os Times filhos desse
- Familia: Coleção de todos os Times pais, filhos e irmãos de um Time. Referente a todos os Times desde o time Raiz até os Times folha
- Descendência: Coleção de todos os filhos, netos, ..., de um time. Referente a todos os times abaixo de um time especifico.

###Ação
Conjunto de instruções que expressão movimentação, fala e expressões do Robô - Diz respeito à programação do robô

- Ticotico: Ação programada usando texto puro e emoticons
- Javascript: Ação programada usando código Javascript - permite a criação de ações complexas

###Gatilho
Trigger - evento responsável por disparar uma ação em um Robô

- Comando de voz: Gatilho de voz, frase em texto à qual o Robô ficará escutando para disparar uma ação correspondente
- Javascript: Gatilho codificado com JS - código javascript executado de tempos em tempos que avalia uma condição qualquer e, quando satisfeita a condição, dispara a ação correspondente
- Agendado: Gatilho repetieivo que dispara uma ação no robô no período agendado - atribui uma ação à um horário do dia, à um dia da semana, etc.

###RobotInfo
Detalhes sobre 1 unidade de robô (número de série, observações da fabrica, versões do app) - dados independendes da versão do hardware do Robô

- Configurações: Configurações de servos, volume, leds e pinos - pode variar com a evolução do hardware

###Contato
Representa uma pessoa que não relação direta com o Portal - utilizado pelo robô para reconhecimento facial ou como lista de contatos

- Lista de humanos: Agrupamento de contatos - utilizado para filtrar contatos especícifos durante o reconhecimento facial ou para limitar o número de contatos durante buscas

###Hook
Responsável pela geração de links para acesso externo ao robô. Tem ligação direta com um time, seja ele uma folha ou não. Dessa forma o usuário, com permissão de Administrador, poderá gerenciar interações diretamente com o robô através dos links gerados

###Variáveis Globais
Grupo de variáveis que poderão ser usados em uma ação, no robô, de um determinado time. Seu cadastro é baseado no nome da chave e no valor para tal chave. 